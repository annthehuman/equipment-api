"""empty message

Revision ID: 2148388d104c
Revises: 
Create Date: 2019-11-11 15:47:12.043435

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2148388d104c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('equip',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('customer_name', sa.String(length=50), nullable=True),
    sa.Column('customer_phone', sa.Integer(), nullable=True),
    sa.Column('equip_serial_number', sa.Integer(), nullable=True),
    sa.Column('equip_name', sa.String(length=50), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('equip')
    # ### end Alembic commands ###
