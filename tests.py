#! python
import unittest
import json
from datetime import datetime
from app import create_app, db
from config import Config


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.equip = {
            "customer_name": "Куприянова Анна Дмитриевна",
            "customer_phone": 89994194205,
            "equip_serial_number": 88005353535,
            "equip_name": "iphone"
        }
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_build_create(self):
        res = self.client().post('/api/equips', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        self.assertIn(self.equip['equip_name'], str(res.data))

    def test_get_all_equips(self):
        res = self.client().post('/api/equips', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/api/equips', headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)
        self.assertIn(self.equip['equip_name'], str(res.data))

    def test_get_equip_by_id(self):
        rv = self.client().post('/api/equips', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'","\""))
        result = self.client().get('/api/equips/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)
        self.assertIn(self.equip['equip_name'], str(result.data))

    def test_equip_can_be_edited(self):
        rv = self.client().post('/api/equips', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        equip_name_old = self.equip['equip_name']
        self.equip['equip_name'] = 'ipod'

        res = self.client().put('/api/equips/{}'.format(result_in_json['id']), data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)

        result = self.client().get('/api/equips/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.equip['equip_name'], str(result.data))
        self.assertNotIn(equip_name_old, str(result.data))

    def test_build_deletion(self):
        rv = self.client().post('/api/equips', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        result = self.client().delete('/api/equips/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)

        result = self.client().get('/api/equips/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 404)

    def test_put_empty_object(self):
        rv = self.client().post('/api/equips', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        build_upd = {}

        res = self.client().put('/api/equips/{}'.format(result_in_json['id']), data=json.dumps(build_upd), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Build should contain something', str(res.data))

        result = self.client().get('/api/equips/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.equip['equip_name'], str(result.data))

    def test_post_empty_object(self):
        res = self.client().post('/api/equips', data=json.dumps({}), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Equipment should contain something', str(res.data))


if __name__ == "__main__":
    unittest.main()
